-- phpMyAdmin SQL Dump
-- version 4.2.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 31, 2014 at 04:46 PM
-- Server version: 5.5.40-MariaDB-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `evremea2`
--

-- --------------------------------------------------------

--
-- Table structure for table `forecastio_api_keys`
--

CREATE TABLE IF NOT EXISTS `forecastio_api_keys` (
`id` int(11) NOT NULL,
  `api_key` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `forecastio_api_keys`
--

INSERT INTO `forecastio_api_keys` (`id`, `api_key`, `email`) VALUES
(1, '43980ce2a3d03e3982647ecc769f2431', 'razvantudorica@gmail.com'),
(2, 'd8cd47d43056cfcd66598dcb0aa9ebda', 'raztud@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `forecastio_calls`
--

CREATE TABLE IF NOT EXISTS `forecastio_calls` (
`id` int(11) NOT NULL,
  `api_key_id` int(11) NOT NULL,
  `day` date NOT NULL,
  `counter` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `forecastio_api_keys`
--
ALTER TABLE `forecastio_api_keys`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forecastio_calls`
--
ALTER TABLE `forecastio_calls`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `api_key_id` (`api_key_id`,`day`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `forecastio_api_keys`
--
ALTER TABLE `forecastio_api_keys`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `forecastio_calls`
--
ALTER TABLE `forecastio_calls`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
