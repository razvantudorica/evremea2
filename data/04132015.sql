UPDATE `city` SET `name` = 'Slanchev Bryag (Sunny Beach)', `ascii_name` = 'Slanchev Bryag (Sunny Beach)' WHERE `city`.`id` = 24474;
UPDATE `city` SET `name` = 'Sv. Konstantin i Elena (Constantin si Elena)',  `ascii_name` = 'Sv. Konstantin i Elena (Constantin si Elena)' WHERE `city`.`id` = 24478;
UPDATE `city` SET `url` = 'constantin-si-elena' WHERE `city`.`id` = 24478;
UPDATE `city` SET `url` = 'sunny-beach-bulgaria' WHERE `city`.`id` = 24474;	
