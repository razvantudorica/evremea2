<?php

namespace Evremea\HomepageBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Evremea\HomepageBundle\Entity\ForecastioCalls;

class ForecastioCallsRepository extends EntityRepository
{
    public function getCallByApiIdAndDate($apiKeyId, $day=NULL)
    {
        if (!$day)
        {
            $day = date('Y-m-d');
        }
        
        $apiCalls = $this->createQueryBuilder('c')
                ->where('c.apiKeyId = :api_key_id')
                ->andWhere('c.day = :day')
                ->setParameter('api_key_id', $apiKeyId)
                ->setParameter('day', $day)
                ->getQuery()->setMaxResults(1)->getResult();
        
        if (count($apiCalls))
        {
            return $apiCalls[0];
        }
        
        return null;
    }
}