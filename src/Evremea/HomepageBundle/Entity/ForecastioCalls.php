<?php

namespace Evremea\HomepageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForecastioCalls
 *
 * @ORM\Table(name="forecastio_calls", uniqueConstraints={@ORM\UniqueConstraint(name="api_key_id", columns={"api_key_id", "day"})})
 * @ORM\Entity(repositoryClass="Evremea\HomepageBundle\Entity\ForecastioCallsRepository")
 */
class ForecastioCalls
{
    /**
     * @var integer
     *
     * @ORM\Column(name="api_key_id", type="integer", nullable=false)
     */
    private $apiKeyId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="day", type="date", nullable=false)
     */
    private $day;

    /**
     * @var integer
     *
     * @ORM\Column(name="counter", type="integer", nullable=false)
     */
    private $counter;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set apiKeyId
     *
     * @param integer $apiKeyId
     * @return ForecastioCalls
     */
    public function setApiKeyId($apiKeyId)
    {
        $this->apiKeyId = $apiKeyId;

        return $this;
    }

    /**
     * Get apiKeyId
     *
     * @return integer 
     */
    public function getApiKeyId()
    {
        return $this->apiKeyId;
    }

    /**
     * Set day
     *
     * @param \DateTime $day
     * @return ForecastioCalls
     */
    public function setDay($day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return \DateTime 
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set counter
     *
     * @param integer $counter
     * @return ForecastioCalls
     */
    public function setCounter($counter)
    {
        $this->counter = $counter;

        return $this;
    }

    /**
     * Get counter
     *
     * @return integer 
     */
    public function getCounter()
    {
        return $this->counter;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
