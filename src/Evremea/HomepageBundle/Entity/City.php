<?php

namespace Evremea\HomepageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * City
 *
 * @ORM\Table(name="city", indexes={@ORM\Index(name="name", columns={"name"}), @ORM\Index(name="url", columns={"url"}), @ORM\Index(name="country_id", columns={"country_id"}), @ORM\Index(name="county_id", columns={"county_id"}), @ORM\Index(name="fcode_name_id", columns={"fcode_name_id"}), @ORM\Index(name="fcl_name_id", columns={"fcl_name_id"}), @ORM\Index(name="ascii_name", columns={"ascii_name"})})
 * @ORM\Entity
 */
class City
{
    /**
     * @var integer
     *
     * @ORM\Column(name="geoname_id", type="bigint", nullable=true)
     */
    private $geonameId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="ascii_name", type="string", length=255, nullable=false)
     */
    private $asciiName;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var integer
     *
     * @ORM\Column(name="country_id", type="integer", nullable=true)
     */
    private $countryId;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=0, nullable=false)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", precision=10, scale=0, nullable=false)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="fcl", type="string", length=5, nullable=false)
     */
    private $fcl;

    /**
     * @var string
     *
     * @ORM\Column(name="fcode", type="string", length=5, nullable=false)
     */
    private $fcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="fcode_name_id", type="integer", nullable=true)
     */
    private $fcodeNameId;

    /**
     * @var integer
     *
     * @ORM\Column(name="fcl_name_id", type="integer", nullable=true)
     */
    private $fclNameId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="gtm_offset", type="boolean", nullable=false)
     */
    private $gtmOffset;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dst_offset", type="boolean", nullable=false)
     */
    private $dstOffset;

    /**
     * @var string
     *
     * @ORM\Column(name="timezone", type="string", length=255, nullable=false)
     */
    private $timezone;

    /**
     * @var integer
     *
     * @ORM\Column(name="population", type="bigint", nullable=false)
     */
    private $population;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Evremea\HomepageBundle\Entity\County
     *
     * @ORM\ManyToOne(targetEntity="Evremea\HomepageBundle\Entity\County")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="county_id", referencedColumnName="id")
     * })
     */
    private $county;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="address_details", type="string", length=2000, nullable=false)
     */
    private $addressDetails;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="google_parsed", type="boolean", nullable=false)
     */
    private $googleParsed;
    
    /**
     * Set geonameId
     *
     * @param integer $geonameId
     * @return City
     */
    public function setGeonameId($geonameId)
    {
        $this->geonameId = $geonameId;

        return $this;
    }

    /**
     * Get geonameId
     *
     * @return integer 
     */
    public function getGeonameId()
    {
        return $this->geonameId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set asciiName
     *
     * @param string $asciiName
     * @return City
     */
    public function setAsciiName($asciiName)
    {
        $this->asciiName = $asciiName;

        return $this;
    }

    /**
     * Get asciiName
     *
     * @return string 
     */
    public function getAsciiName()
    {
        return $this->asciiName;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return City
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set countryId
     *
     * @param integer $countryId
     * @return City
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * Get countryId
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return City
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return City
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set fcl
     *
     * @param string $fcl
     * @return City
     */
    public function setFcl($fcl)
    {
        $this->fcl = $fcl;

        return $this;
    }

    /**
     * Get fcl
     *
     * @return string 
     */
    public function getFcl()
    {
        return $this->fcl;
    }

    /**
     * Set fcode
     *
     * @param string $fcode
     * @return City
     */
    public function setFcode($fcode)
    {
        $this->fcode = $fcode;

        return $this;
    }

    /**
     * Get fcode
     *
     * @return string 
     */
    public function getFcode()
    {
        return $this->fcode;
    }

    /**
     * Set fcodeNameId
     *
     * @param integer $fcodeNameId
     * @return City
     */
    public function setFcodeNameId($fcodeNameId)
    {
        $this->fcodeNameId = $fcodeNameId;

        return $this;
    }

    /**
     * Get fcodeNameId
     *
     * @return integer 
     */
    public function getFcodeNameId()
    {
        return $this->fcodeNameId;
    }

    /**
     * Set fclNameId
     *
     * @param integer $fclNameId
     * @return City
     */
    public function setFclNameId($fclNameId)
    {
        $this->fclNameId = $fclNameId;

        return $this;
    }

    /**
     * Get fclNameId
     *
     * @return integer 
     */
    public function getFclNameId()
    {
        return $this->fclNameId;
    }

    /**
     * Set gtmOffset
     *
     * @param boolean $gtmOffset
     * @return City
     */
    public function setGtmOffset($gtmOffset)
    {
        $this->gtmOffset = $gtmOffset;

        return $this;
    }

    /**
     * Get gtmOffset
     *
     * @return boolean 
     */
    public function getGtmOffset()
    {
        return $this->gtmOffset;
    }

    /**
     * Set dstOffset
     *
     * @param boolean $dstOffset
     * @return City
     */
    public function setDstOffset($dstOffset)
    {
        $this->dstOffset = $dstOffset;

        return $this;
    }

    /**
     * Get dstOffset
     *
     * @return boolean 
     */
    public function getDstOffset()
    {
        return $this->dstOffset;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     * @return City
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string 
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set population
     *
     * @param integer $population
     * @return City
     */
    public function setPopulation($population)
    {
        $this->population = $population;

        return $this;
    }

    /**
     * Get population
     *
     * @return integer 
     */
    public function getPopulation()
    {
        return $this->population;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set county
     *
     * @param \Evremea\HomepageBundle\Entity\County $county
     * @return City
     */
    public function setCounty(\Evremea\HomepageBundle\Entity\County $county = null)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * Get county
     *
     * @return \Evremea\HomepageBundle\Entity\County 
     */
    public function getCounty()
    {
        return $this->county;
    }
    
    /**
     * Set geonameId
     *
     * @param string $addressDetails
     * @return City
     */
    public function setAddressDetails($addressDetails)
    {
        $this->addressDetails = $addressDetails;

        return $this;
    }

    /**
     * Get addressDetails
     *
     * @return string
     */
    public function getAddressDetails()
    {
        return $this->addressDetails;
    }
    
    /**
     * Set googleParsed
     *
     * @param boolean $googleParsed
     * @return City
     */
    public function setGoogleParsed($googleParsed)
    {
        $this->googleParsed = $googleParsed;

        return $this;
    }

    /**
     * Get googleParsed
     *
     * @return boolean 
     */
    public function getGoogleParsed()
    {
        return $this->googleParsed;
    }
}
