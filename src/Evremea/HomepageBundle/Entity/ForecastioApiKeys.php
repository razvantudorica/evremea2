<?php

namespace Evremea\HomepageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ForecastioApiKeys
 *
 * @ORM\Table(name="forecastio_api_keys")
 * @ORM\Entity(repositoryClass="Evremea\HomepageBundle\Entity\ForecastioApiKeysRepository")
 * 
 */
class ForecastioApiKeys
{
    /**
     * @var string
     *
     * @ORM\Column(name="api_key", type="string", length=255, nullable=false)
     */
    private $apiKey;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set apiKey
     *
     * @param string $apiKey
     * @return ForecastioApiKeys
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * Get apiKey
     *
     * @return string 
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return ForecastioApiKeys
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
