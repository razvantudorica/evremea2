<?php

namespace Evremea\HomepageBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Evremea\HomepageBundle\Entity\ForecastioCalls;

class ForecastioApiKeysRepository extends EntityRepository
{
    const MAX_CALLS = 800;
    /**
     * It will return an API key with less than 1000 request for today
     */
    public function getAvailableKey()
    {
        $manager = $this->getEntityManager();
        
        $today = date('Y-m-d');
        $dql = "SELECT c FROM EvremeaHomepageBundle:ForecastioCalls c WHERE c.day='" . $today . "'";
        
        //search if the current date is in the table
        $apiCalls = $manager->createQuery($dql)->getResult();
        if (!count($apiCalls))
        {
            //if the no call was done today; just insert 1st key
            //select 1 and insert it
            $dql = "SELECT k FROM EvremeaHomepageBundle:ForecastioApiKeys k";
            $apiKeys = $manager->createQuery($dql)->setMaxResults(1)->getResult();
            
            foreach ($apiKeys as $apiKey)
            {
                $apiKeyId = $apiKey->getId();
                
                $call = new ForecastioCalls();
                $call->setApiKeyId($apiKeyId);
                $call->setDay(new \DateTime());
                $call->setCounter(0);
                
                $manager->persist($call);
                $manager->flush();
                
                return $apiKey->getApiKey();
            }
        }
        else 
        {
            //some calls where done today
            //we check if we still have calls for the current keys
            $usedKeys = array();
            foreach ($apiCalls as $apiCall)
            {
                $usedKeys[] = $apiCall->getApiKeyId();
                if ($apiCall->getCounter() < ForecastioApiKeysRepository::MAX_CALLS)
                {
                    $repository = $manager->getRepository('EvremeaHomepageBundle:ForecastioApiKeys');
                    return $repository->find($apiCall->getApiKeyId())->getApiKey();
                }
            }
            
        }
        
        //get another one
        $key = $this->getKeyExceptIds($usedKeys);
        if ($key)
        {
            $call = new ForecastioCalls();
            $call->setApiKeyId($key->getId());
            $call->setDay(new \DateTime());
            $call->setCounter(0);

            $manager->persist($call);
            $manager->flush();

            return $key->getApiKey();
        }
        
        return null;
    }
    
    public function getKeyExceptIds($ids=array())
    {
        $manager = $this->getEntityManager();
        $repository = $manager->getRepository('EvremeaHomepageBundle:ForecastioApiKeys');
        
        $query = $repository->createQueryBuilder('k')
            ->where('k.id NOT IN (:ids)')
            ->setParameter('ids', $ids)
            ->setMaxResults(1)
            ->getQuery();
        
        return $query->getOneOrNullResult();
        
    }
}
