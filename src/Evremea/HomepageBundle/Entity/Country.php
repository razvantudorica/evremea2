<?php

namespace Evremea\HomepageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Country
 *
 * @ORM\Table(name="country", indexes={@ORM\Index(name="url", columns={"url"}), @ORM\Index(name="name", columns={"name"})})
 * @ORM\Entity
 */
class Country
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=5, nullable=false)
     */
    private $code;

    /**
     * @var integer
     *
     * @ORM\Column(name="geoname_id", type="bigint", nullable=true)
     */
    private $geonameId;

    /**
     * @var string
     *
     * @ORM\Column(name="toponym", type="string", length=255, nullable=false)
     */
    private $toponym;

    /**
     * @var integer
     *
     * @ORM\Column(name="population", type="integer", nullable=true)
     */
    private $population;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float", precision=10, scale=0, nullable=false)
     */
    private $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="long", type="float", precision=10, scale=0, nullable=false)
     */
    private $long;

    /**
     * @var string
     *
     * @ORM\Column(name="fcl", type="string", length=5, nullable=false)
     */
    private $fcl;

    /**
     * @var string
     *
     * @ORM\Column(name="fcode", type="string", length=5, nullable=false)
     */
    private $fcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="fcode_name_id", type="integer", nullable=false)
     */
    private $fcodeNameId;

    /**
     * @var integer
     *
     * @ORM\Column(name="fcl_name_id", type="integer", nullable=false)
     */
    private $fclNameId;

    /**
     * @var float
     *
     * @ORM\Column(name="gtm_offset", type="float", precision=10, scale=0, nullable=false)
     */
    private $gtmOffset;

    /**
     * @var float
     *
     * @ORM\Column(name="dst_offset", type="float", precision=10, scale=0, nullable=false)
     */
    private $dstOffset;

    /**
     * @var string
     *
     * @ORM\Column(name="timezone", type="string", length=255, nullable=false)
     */
    private $timezone;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set name
     *
     * @param string $name
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Country
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set geonameId
     *
     * @param integer $geonameId
     * @return Country
     */
    public function setGeonameId($geonameId)
    {
        $this->geonameId = $geonameId;

        return $this;
    }

    /**
     * Get geonameId
     *
     * @return integer 
     */
    public function getGeonameId()
    {
        return $this->geonameId;
    }

    /**
     * Set toponym
     *
     * @param string $toponym
     * @return Country
     */
    public function setToponym($toponym)
    {
        $this->toponym = $toponym;

        return $this;
    }

    /**
     * Get toponym
     *
     * @return string 
     */
    public function getToponym()
    {
        return $this->toponym;
    }

    /**
     * Set population
     *
     * @param integer $population
     * @return Country
     */
    public function setPopulation($population)
    {
        $this->population = $population;

        return $this;
    }

    /**
     * Get population
     *
     * @return integer 
     */
    public function getPopulation()
    {
        return $this->population;
    }

    /**
     * Set lat
     *
     * @param float $lat
     * @return Country
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return float 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set long
     *
     * @param float $long
     * @return Country
     */
    public function setLong($long)
    {
        $this->long = $long;

        return $this;
    }

    /**
     * Get long
     *
     * @return float 
     */
    public function getLong()
    {
        return $this->long;
    }

    /**
     * Set fcl
     *
     * @param string $fcl
     * @return Country
     */
    public function setFcl($fcl)
    {
        $this->fcl = $fcl;

        return $this;
    }

    /**
     * Get fcl
     *
     * @return string 
     */
    public function getFcl()
    {
        return $this->fcl;
    }

    /**
     * Set fcode
     *
     * @param string $fcode
     * @return Country
     */
    public function setFcode($fcode)
    {
        $this->fcode = $fcode;

        return $this;
    }

    /**
     * Get fcode
     *
     * @return string 
     */
    public function getFcode()
    {
        return $this->fcode;
    }

    /**
     * Set fcodeNameId
     *
     * @param integer $fcodeNameId
     * @return Country
     */
    public function setFcodeNameId($fcodeNameId)
    {
        $this->fcodeNameId = $fcodeNameId;

        return $this;
    }

    /**
     * Get fcodeNameId
     *
     * @return integer 
     */
    public function getFcodeNameId()
    {
        return $this->fcodeNameId;
    }

    /**
     * Set fclNameId
     *
     * @param integer $fclNameId
     * @return Country
     */
    public function setFclNameId($fclNameId)
    {
        $this->fclNameId = $fclNameId;

        return $this;
    }

    /**
     * Get fclNameId
     *
     * @return integer 
     */
    public function getFclNameId()
    {
        return $this->fclNameId;
    }

    /**
     * Set gtmOffset
     *
     * @param float $gtmOffset
     * @return Country
     */
    public function setGtmOffset($gtmOffset)
    {
        $this->gtmOffset = $gtmOffset;

        return $this;
    }

    /**
     * Get gtmOffset
     *
     * @return float 
     */
    public function getGtmOffset()
    {
        return $this->gtmOffset;
    }

    /**
     * Set dstOffset
     *
     * @param float $dstOffset
     * @return Country
     */
    public function setDstOffset($dstOffset)
    {
        $this->dstOffset = $dstOffset;

        return $this;
    }

    /**
     * Get dstOffset
     *
     * @return float 
     */
    public function getDstOffset()
    {
        return $this->dstOffset;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     * @return Country
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string 
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Country
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function __toString() {
        return $this->name;
    }
}
