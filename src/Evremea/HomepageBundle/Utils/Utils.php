<?php

namespace Evremea\HomepageBundle\Utils;

use Evremea\HomepageBundle\Utils\GeoIp\EvremeaGeoIp;
use Evremea\HomepageBundle\Utils\GeoIp\City;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Utils
{
    
    public static function getUserIP()
    {
        /**
         * It returns the IP or false if is private
         */
        
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }
      
        return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE |  FILTER_FLAG_NO_RES_RANGE);
    }
    
    public static function getCityByIpGeoplugin($ip, $httpClient) 
    {
//        $log = new Logger('name');
//        $log->pushHandler(new StreamHandler('/tmp/geoip.log', Logger::WARNING));
//        $log->addError('TEST');
//        
        $city = new City();
        
        if (!$ip)
        {
            return $city->setDefault(); //Bucharest
        }
        
        $httpClient->setBaseUrl('http://www.geoplugin.net/');
        $resource = 'json.gp?' . $ip;
        
        $request = $httpClient->get($resource);
        $res = $request->send();
        if ($res->getStatusCode() != 200)
        {
            throw new \Exception("Could not fetch the response");
        }
        
        $data = $res->json();
        
        $city->latitude = $data['geoplugin_latitude'];
        $city->longitude = $data['geoplugin_longitude'];
        $city->city = $data['geoplugin_city'];
        $city->country_name = $data['geoplugin_countryName'];
        $city->region = $data['geoplugin_region'];
        
        return $city;
    }
    
    public static function getCityByIp($ip) 
    {
        $geoIpPath = '/usr/local/share/GeoIP/GeoIPCity.dat';
        $city = new City();
        
        if (!$ip)
        {
            return $city->setDefault(); //Bucharest
        }
        
        $geoip = new EvremeaGeoIp($geoIpPath);
        
        try
        {
	    $result = $geoip->geoip_record_by_addr($ip);
	    if (!$result) {
                throw new \Exception();
            }

            $city->latitude = $result->latitude;
            $city->longitude = $result->longitude;
            $city->city = $result->city ? $result->city : NULL;
        }
        catch (\Exception $e)
        {
            return $city->setDefault(); //Bucharest
        }
        try
        {
            $city->country_name = $result->country_name ? $result->country_name : '';
        }
        catch(\Exception $e)
        {
            $city->country_name = 'N/A';
        }
        
        try
        {
            $city->region = $result->region;
        }
        catch(\Exception $e)
        {
            $city->region = 'N/A';
        }
        
        return $city;
    }
    
    public static function dayOrNight()
    {
        $h = date('H');
        if ($h > 18 || $h<7)
        {
            return 'night';
        }
        return 'day';
    }
    
    public static function getBiggestCities()
    {
        return array('Bucuresti'=>'bucuresti', 'Cluj-Napoca'=>'cluj-napoca', 'Timișoara'=>'timisoara', 'Iași'=>'iasi-1', 
            'Constanța'=>'constanta', 'Craiova'=>'craiova', 'Brașov'=>'brasov', 'Galați'=>'galati', 'Ploiești'=>'ploiesti', 'Oradea'=>'oradea', 
            'Brăila'=>'braila', 'Arad'=>'arad', 'Sibiu'=>'sibiu', 'Bacău'=>'bacau', 'Târgu-Mureș'=>'targu-mures', 'Baia Mare'=>'baia-mare-1', 
            'Buzău'=>'buzau', 'Botoșani'=>'botosani', 'Satu-Mare'=>'satu-mare-4', 'Râmnicu-Vâlcea'=>'ramnicu-valcea');
    }
    
    public static function getIconFromData($json)
    {
        $icon = $json['currently']['icon'];
        
        if ($icon == 'partly-cloudy-day')
        {
            $icon = 'cloudy-day';
        }
        
        if ($icon == 'partly-cloudy-night')
        {
            $icon = 'cloudy-night';
        }
        
        if ($icon == 'clear' || $icon == 'cloudy')
        {
            $icon .= '-' . Utils::dayOrNight();
        }
        
        return $icon;
    }
    
    public static function translator($text)
    {
        $text = strtolower($text);

        //what
        $text = str_replace('mixed precipitation', 'precipitații mixte', $text);
        $text = str_replace('light precipitation', 'precipitații ușoare', $text);
        $text = str_replace('heavy precipitation', 'precipitații puternice', $text);
        $text = str_replace('precipitation', 'precipitații', $text);
        
        $text = str_replace('possible', 'posibil', $text);
        
        $text = str_replace('heavy snow', 'ninsoare slabă', $text);
        $text = str_replace('light snow', 'ninsoare slabă', $text);
        $text = str_replace('snow', 'ninsoare', $text);
        
        $text = str_replace('heavy rain', 'ploaie puternică', $text); 
        $text = str_replace('light rain', 'ploaie slabă', $text);
        $text = str_replace('rain', 'ploaie', $text);
        
        $text = str_replace('heavy sleet', 'lapoviță puternică', $text); 
        $text = str_replace('light sleet', 'lapoviță slabă', $text);
        $text = str_replace('sleet', 'lapoviță', $text);
        
        $text = str_replace('light breezy', 'adiere de vânt slabă', $text);
        $text = str_replace('breezy', 'adiere de vânt', $text);
        $text = str_replace('windy', 'vânt', $text);
        $text = str_replace('dangerously windy', 'vânt periculos', $text);
        
        
        $text = str_replace('mostly', 'Predominant', $text);
        $text = str_replace('partly', 'parțial', $text);
        
        $text = str_replace('cloudy', 'înnorat', $text);
        $text = str_replace('clear', 'senin', $text);
        $text = str_replace('flurries', 'rafale', $text);
        $text = str_replace('drizzle', 'burniță', $text);

        //pana
        $text = str_replace('until', 'până', $text);
        $text = str_replace('throughout', 'în timpul', $text);
        $text = str_replace('starting', 'începând', $text);
        $text = str_replace('continuing', 'continuând', $text);

        //cand
        $text = str_replace('in the afternoon', 'după-amiaza', $text);
        $text = str_replace('in the evening', 'seara', $text);
        $text = str_replace('in the morning', 'dimineața', $text);
        $text = str_replace('overnight', 'în timpul nopții', $text);
        $text = str_replace('starting again', 'începând din nou', $text);
        $text = str_replace('the day', 'zilei', $text);
        
        $text = str_replace('afternoon', 'după-amiaza', $text);
        $text = str_replace('evening', 'seara', $text);
        $text = str_replace('morning', 'dimineață', $text);
        $text = str_replace('night', 'noaptea', $text);
        
        $text = str_replace('under', 'sub', $text);
        $text = str_replace('and', 'și', $text);
        $text = str_replace('again', 'din nou', $text);

        $text = str_replace('dry', 'uscat', $text);
        $text = str_replace('humid', 'umed', $text);
        $text = str_replace('foggy', 'ceață', $text);
        $text = str_replace('overcast', 'acoperit de nori', $text);
        $text = str_replace('later', 'mai târziu', $text);
        $text = str_replace('this', 'în această', $text);
        $text = str_replace('tomorrow', 'mâine', $text);
        
        return ucfirst($text);
    }
    
    public static function isBot()
    {
        $spiders = array(
            "abot",
            "dbot",
            "ebot",
            "hbot",
            "kbot",
            "lbot",
            "mbot",
            "nbot",
            "obot",
            "pbot",
            "rbot",
            "sbot",
            "tbot",
            "vbot",
            "ybot",
            "zbot",
            "bot.",
            "bot/",
            "_bot",
            ".bot",
            "/bot",
            "-bot",
            ":bot",
            "(bot",
            "crawl",
            "slurp",
            "spider",
            "seek",
            "accoona",
            "acoon",
            "adressendeutschland",
            "ah-ha.com",
            "ahoy",
            "altavista",
            "ananzi",
            "anthill",
            "appie",
            "arachnophilia",
            "arale",
            "araneo",
            "aranha",
            "architext",
            "aretha",
            "arks",
            "asterias",
            "atlocal",
            "atn",
            "atomz",
            "augurfind",
            "backrub",
            "bannana_bot",
            "baypup",
            "bdfetch",
            "big brother",
            "biglotron",
            "bjaaland",
            "blackwidow",
            "blaiz",
            "blog",
            "blo.",
            "bloodhound",
            "boitho",
            "booch",
            "bradley",
            "butterfly",
            "calif",
            "cassandra",
            "ccubee",
            "cfetch",
            "charlotte",
            "churl",
            "cienciaficcion",
            "cmc",
            "collective",
            "comagent",
            "combine",
            "computingsite",
            "csci",
            "curl",
            "cusco",
            "daumoa",
            "deepindex",
            "delorie",
            "depspid",
            "deweb",
            "die blinde kuh",
            "digger",
            "ditto",
            "dmoz",
            "docomo",
            "download express",
            "dtaagent",
            "dwcp",
            "ebiness",
            "ebingbong",
            "e-collector",
            "ejupiter",
            "emacs-w3 search engine",
            "esther",
            "evliya celebi",
            "ezresult",
            "falcon",
            "felix ide",
            "ferret",
            "fetchrover",
            "fido",
            "findlinks",
            "fireball",
            "fish search",
            "fouineur",
            "funnelweb",
            "gazz",
            "gcreep",
            "genieknows",
            "getterroboplus",
            "geturl",
            "glx",
            "goforit",
            "golem",
            "grabber",
            "grapnel",
            "gralon",
            "griffon",
            "gromit",
            "grub",
            "gulliver",
            "hamahakki",
            "harvest",
            "havindex",
            "helix",
            "heritrix",
            "hku www octopus",
            "homerweb",
            "htdig",
            "html index",
            "html_analyzer",
            "htmlgobble",
            "hubater",
            "hyper-decontextualizer",
            "ia_archiver",
            "ibm_planetwide",
            "ichiro",
            "iconsurf",
            "iltrovatore",
            "image.kapsi.net",
            "imagelock",
            "incywincy",
            "indexer",
            "infobee",
            "informant",
            "ingrid",
            "inktomisearch.com",
            "inspector web",
            "intelliagent",
            "internet shinchakubin",
            "ip3000",
            "iron33",
            "israeli-search",
            "ivia",
            "jack",
            "jakarta",
            "javabee",
            "jetbot",
            "jumpstation",
            "katipo",
            "kdd-explorer",
            "kilroy",
            "knowledge",
            "kototoi",
            "kretrieve",
            "labelgrabber",
            "lachesis",
            "larbin",
            "legs",
            "libwww",
            "linkalarm",
            "link validator",
            "linkscan",
            "lockon",
            "lwp",
            "lycos",
            "magpie",
            "mantraagent",
            "mapoftheinternet",
            "marvin/",
            "mattie",
            "mediafox",
            "mediapartners",
            "mercator",
            "merzscope",
            "microsoft url control",
            "minirank",
            "miva",
            "mj12",
            "mnogosearch",
            "moget",
            "monster",
            "moose",
            "motor",
            "multitext",
            "muncher",
            "muscatferret",
            "mwd.search",
            "myweb",
            "najdi",
            "nameprotect",
            "nationaldirectory",
            "nazilla",
            "ncsa beta",
            "nec-meshexplorer",
            "nederland.zoek",
            "netcarta webmap engine",
            "netmechanic",
            "netresearchserver",
            "netscoop",
            "newscan-online",
            "nhse",
            "nokia6682/",
            "nomad",
            "noyona",
            "nutch",
            "nzexplorer",
            "objectssearch",
            "occam",
            "omni",
            "open text",
            "openfind",
            "openintelligencedata",
            "orb search",
            "osis-project",
            "pack rat",
            "pageboy",
            "pagebull",
            "page_verifier",
            "panscient",
            "parasite",
            "partnersite",
            "patric",
            "pear.",
            "pegasus",
            "peregrinator",
            "pgp key agent",
            "phantom",
            "phpdig",
            "picosearch",
            "piltdownman",
            "pimptrain",
            "pinpoint",
            "pioneer",
            "piranha",
            "plumtreewebaccessor",
            "pogodak",
            "poirot",
            "pompos",
            "poppelsdorf",
            "poppi",
            "popular iconoclast",
            "psycheclone",
            "publisher",
            "python",
            "rambler",
            "raven search",
            "roach",
            "road runner",
            "roadhouse",
            "robbie",
            "robofox",
            "robozilla",
            "rules",
            "salty",
            "sbider",
            "scooter",
            "scoutjet",
            "scrubby",
            "search.",
            "searchprocess",
            "semanticdiscovery",
            "senrigan",
            "sg-scout",
            "shai'hulud",
            "shark",
            "shopwiki",
            "sidewinder",
            "sift",
            "silk",
            "simmany",
            "site searcher",
            "site valet",
            "sitetech-rover",
            "skymob.com",
            "sleek",
            "smartwit",
            "sna-",
            "snappy",
            "snooper",
            "sohu",
            "speedfind",
            "sphere",
            "sphider",
            "spinner",
            "spyder",
            "steeler/",
            "suke",
            "suntek",
            "supersnooper",
            "surfnomore",
            "sven",
            "sygol",
            "szukacz",
            "tach black widow",
            "tarantula",
            "templeton",
            "/teoma",
            "t-h-u-n-d-e-r-s-t-o-n-e",
            "theophrastus",
            "titan",
            "titin",
            "tkwww",
            "toutatis",
            "t-rex",
            "tutorgig",
            "twiceler",
            "twisted",
            "ucsd",
            "udmsearch",
            "url check",
            "updated",
            "vagabondo",
            "valkyrie",
            "verticrawl",
            "victoria",
            "vision-search",
            "volcano",
            "voyager/",
            "voyager-hc",
            "w3c_validator",
            "w3m2",
            "w3mir",
            "walker",
            "wallpaper",
            "wanderer",
            "wauuu",
            "wavefire",
            "web core",
            "web hopper",
            "web wombat",
            "webbandit",
            "webcatcher",
            "webcopy",
            "webfoot",
            "weblayers",
            "weblinker",
            "weblog monitor",
            "webmirror",
            "webmonkey",
            "webquest",
            "webreaper",
            "websitepulse",
            "websnarf",
            "webstolperer",
            "webvac",
            "webwalk",
            "webwatch",
            "webwombat",
            "webzinger",
            "wget",
            "whizbang",
            "whowhere",
            "wild ferret",
            "worldlight",
            "wwwc",
            "wwwster",
            "xenu",
            "xget",
            "xift",
            "xirq",
            "yandex",
            "yanga",
            "yeti",
            "yodao",
            "zao/",
            "zippp",
            "zyborg",
        );

        foreach($spiders as $spider) {
            //If the spider text is found in the current user agent, then return true
            if ( stripos($_SERVER['HTTP_USER_AGENT'], $spider) !== false ) return true;
        }
        //If it gets this far then no bot was found!
        return false;
    }
}
