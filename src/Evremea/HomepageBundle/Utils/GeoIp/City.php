<?php

namespace Evremea\HomepageBundle\Utils\GeoIp;


class City
{
    public $longitude;
    public $latitude;
    public $city;
    public $country_name;
    public $region;
    public $address_details;
    
    public function setDefault()
    {
        $this->city = 'București';
        $this->country_name = 'România';        
        $this->latitude = 44.438683;
        $this->longitude = 26.102829;
        $this->region = 'București';
        $this->address_details = 'București';
        
        return $this;
    }
}