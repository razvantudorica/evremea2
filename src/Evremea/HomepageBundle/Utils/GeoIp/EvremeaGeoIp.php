<?php

/**
 * Helper Class for GeoIP
 * 
 * 
 */


namespace Evremea\HomepageBundle\Utils\GeoIp;

use Evremea\HomepageBundle\Utils\GeoIp\GeoIp;
use Evremea\HomepageBundle\Utils\GeoIp\GeoIpRecord;

define("GEOIP_COUNTRY_BEGIN", 16776960);
define("GEOIP_STATE_BEGIN_REV0", 16700000);
define("GEOIP_STATE_BEGIN_REV1", 16000000);
define("GEOIP_STANDARD", 0);
define("GEOIP_MEMORY_CACHE", 1);
define("GEOIP_SHARED_MEMORY", 2);
define("STRUCTURE_INFO_MAX_SIZE", 20);
define("DATABASE_INFO_MAX_SIZE", 100);
define("GEOIP_COUNTRY_EDITION", 1);
define("GEOIP_PROXY_EDITION", 8);
define("GEOIP_ASNUM_EDITION", 9);
define("GEOIP_NETSPEED_EDITION", 10);
define("GEOIP_REGION_EDITION_REV0", 7);
define("GEOIP_REGION_EDITION_REV1", 3);
define("GEOIP_CITY_EDITION_REV0", 6);
define("GEOIP_CITY_EDITION_REV1", 2);
define("GEOIP_ORG_EDITION", 5);
define("GEOIP_ISP_EDITION", 4);
define("SEGMENT_RECORD_LENGTH", 3);
define("STANDARD_RECORD_LENGTH", 3);
define("ORG_RECORD_LENGTH", 4);
define("MAX_RECORD_LENGTH", 4);
define("MAX_ORG_RECORD_LENGTH", 300);
define("GEOIP_SHM_KEY", 0x4f415401);
define("US_OFFSET", 1);
define("CANADA_OFFSET", 677);
define("WORLD_OFFSET", 1353);
define("FIPS_RANGE", 360);
define("GEOIP_UNKNOWN_SPEED", 0);
define("GEOIP_DIALUP_SPEED", 1);
define("GEOIP_CABLEDSL_SPEED", 2);
define("GEOIP_CORPORATE_SPEED", 3);
define("GEOIP_DOMAIN_EDITION", 11);
define("GEOIP_COUNTRY_EDITION_V6", 12);
define("GEOIP_LOCATIONA_EDITION", 13);
define("GEOIP_ACCURACYRADIUS_EDITION", 14);
define("GEOIP_CITYCOMBINED_EDITION", 15);
define("GEOIP_CITY_EDITION_REV1_V6", 30);
define("GEOIP_CITY_EDITION_REV0_V6", 31);
define("GEOIP_NETSPEED_EDITION_REV1", 32);
define("GEOIP_NETSPEED_EDITION_REV1_V6", 33);
define("GEOIP_USERTYPE_EDITION", 28);
define("GEOIP_USERTYPE_EDITION_V6", 29);
define("GEOIP_ASNUM_EDITION_V6", 21);
define("GEOIP_ISP_EDITION_V6", 22);
define("GEOIP_ORG_EDITION_V6", 23);
define("GEOIP_DOMAIN_EDITION_V6", 24);

define("CITYCOMBINED_FIXED_RECORD", 7);

define("FULL_RECORD_LENGTH", 50);

class EvremeaGeoIp
{
    
    public function __construct($geoIpDatabase=null) 
    {        
        if (!is_null($geoIpDatabase))
        {
            $this->gi = $this->_geoip_open($geoIpDatabase, GEOIP_STANDARD);
        }
        else 
        {
            $this->gi = $this->_geoip_open("/usr/local/share/GeoIP/GeoIPCity.dat", GEOIP_STANDARD);
        }
    }
    
    private function _geoip_open($filename, $flags)
    {
        $gi = new GeoIp();
        $gi->flags = $flags;
        if ($gi->flags & GEOIP_SHARED_MEMORY) {
            $gi->shmid = @shmop_open(GEOIP_SHM_KEY, "a", 0, 0);
        } else {
            $gi->filehandle = fopen($filename, "rb");
            if (!$gi->filehandle)
            {
                throw new \Exception ("GeoIP API: Can not open $filename\n", E_USER_ERROR);
            }
            if ($gi->flags & GEOIP_MEMORY_CACHE) {
                $s_array = fstat($gi->filehandle);
                $gi->memory_buffer = fread($gi->filehandle, $s_array['size']);
            }
        }

        $gi = $this->_setup_segments($gi);
        return $gi;
    }
    
    public function geoip_record_by_addr($addr)
    {
        if ($addr == null) {
            return 0;
        }
        $ipnum = ip2long($addr);
        return $this->_get_record($this->gi, $ipnum);
    }
    
    private function _get_record($gi, $ipnum)
    {
        $seek_country = $this->_geoip_seek_country($gi, $ipnum);
        if ($seek_country == $gi->databaseSegments) {
            return null;
        }
        return $this->_common_get_record($gi, $seek_country);
    }
    
    private function _geoip_seek_country($gi, $ipnum)
    {
        $offset = 0;
        for ($depth = 31; $depth >= 0; --$depth) {
            if ($gi->flags & GEOIP_MEMORY_CACHE) {
                $buf = _safe_substr(
                    $gi->memory_buffer,
                    2 * $gi->record_length * $offset,
                    2 * $gi->record_length
                );
            } elseif ($gi->flags & GEOIP_SHARED_MEMORY) {
                $buf = @shmop_read(
                    $gi->shmid,
                    2 * $gi->record_length * $offset,
                    2 * $gi->record_length
                );
            } else {
                
                if (fseek($gi->filehandle, 2 * $gi->record_length * $offset, SEEK_SET) != 0)
                {
                    throw new \Exception ("GeoIP API: fseek failed", E_USER_ERROR);
                }
                $buf = fread($gi->filehandle, 2 * $gi->record_length);
            }
            $x = array(0, 0);
            for ($i = 0; $i < 2; ++$i) {
                for ($j = 0; $j < $gi->record_length; ++$j) {
                    $x[$i] += ord($buf[$gi->record_length * $i + $j]) << ($j * 8);
                }
            }
            if ($ipnum & (1 << $depth)) {
                if ($x[1] >= $gi->databaseSegments) {
                    return $x[1];
                }
                $offset = $x[1];
            } else {
                if ($x[0] >= $gi->databaseSegments) {
                    return $x[0];
                }
                $offset = $x[0];
            }
        }
        
        throw new \Exception("GeoIP API: Error traversing database - perhaps it is corrupt?", E_USER_ERROR);
        
    }
    
    private function _common_get_record($gi, $seek_country)
    {
        // workaround php's broken substr, strpos, etc handling with
        // mbstring.func_overload and mbstring.internal_encoding
        $mbExists = extension_loaded('mbstring');
        if ($mbExists) {
            $enc = mb_internal_encoding();
            mb_internal_encoding('ISO-8859-1');
        }

        $record_pointer = $seek_country + (2 * $gi->record_length - 1) * $gi->databaseSegments;

        if ($gi->flags & GEOIP_MEMORY_CACHE) {
            $record_buf = substr($gi->memory_buffer, $record_pointer, FULL_RECORD_LENGTH);
        } elseif ($gi->flags & GEOIP_SHARED_MEMORY) {
            $record_buf = @shmop_read($gi->shmid, $record_pointer, FULL_RECORD_LENGTH);
        } else {
            fseek($gi->filehandle, $record_pointer, SEEK_SET);
            $record_buf = fread($gi->filehandle, FULL_RECORD_LENGTH);
        }
        $record = new GeoIpRecord();
        $record_buf_pos = 0;
        $char = ord(substr($record_buf, $record_buf_pos, 1));
        $record->country_code = $gi->GEOIP_COUNTRY_CODES[$char];
        $record->country_code3 = $gi->GEOIP_COUNTRY_CODES3[$char];
        $record->country_name = $gi->GEOIP_COUNTRY_NAMES[$char];
        $record->continent_code = $gi->GEOIP_CONTINENT_CODES[$char];
        $record_buf_pos++;
        $str_length = 0;

        // Get region
        $char = ord(substr($record_buf, $record_buf_pos + $str_length, 1));
        while ($char != 0) {
            $str_length++;
            $char = ord(substr($record_buf, $record_buf_pos + $str_length, 1));
        }
        if ($str_length > 0) {
            $record->region = substr($record_buf, $record_buf_pos, $str_length);
        }
        $record_buf_pos += $str_length + 1;
        $str_length = 0;
        // Get city
        $char = ord(substr($record_buf, $record_buf_pos + $str_length, 1));
        while ($char != 0) {
            $str_length++;
            $char = ord(substr($record_buf, $record_buf_pos + $str_length, 1));
        }
        if ($str_length > 0) {
            $record->city = substr($record_buf, $record_buf_pos, $str_length);
        }
        $record_buf_pos += $str_length + 1;
        $str_length = 0;
        // Get postal code
        $char = ord(substr($record_buf, $record_buf_pos + $str_length, 1));
        while ($char != 0) {
            $str_length++;
            $char = ord(substr($record_buf, $record_buf_pos + $str_length, 1));
        }
        if ($str_length > 0) {
            $record->postal_code = substr($record_buf, $record_buf_pos, $str_length);
        }
        $record_buf_pos += $str_length + 1;
        $str_length = 0;
        // Get latitude and longitude
        $latitude = 0;
        $longitude = 0;
        for ($j = 0; $j < 3; ++$j) {
            $char = ord(substr($record_buf, $record_buf_pos++, 1));
            $latitude += ($char << ($j * 8));
        }
        $record->latitude = ($latitude / 10000) - 180;
        for ($j = 0; $j < 3; ++$j) {
            $char = ord(substr($record_buf, $record_buf_pos++, 1));
            $longitude += ($char << ($j * 8));
        }
        $record->longitude = ($longitude / 10000) - 180;
        if (GEOIP_CITY_EDITION_REV1 == $gi->databaseType) {
            $metroarea_combo = 0;
            if ($record->country_code == "US") {
                for ($j = 0; $j < 3; ++$j) {
                    $char = ord(substr($record_buf, $record_buf_pos++, 1));
                    $metroarea_combo += ($char << ($j * 8));
                }
                $record->metro_code = $record->dma_code = floor($metroarea_combo / 1000);
                $record->area_code = $metroarea_combo % 1000;
            }
        }
        if ($mbExists) {
            mb_internal_encoding($enc);
        }
        return $record;
    }
    
    private function _setup_segments($gi)
    {
        $gi->databaseType = GEOIP_COUNTRY_EDITION;
        $gi->record_length = STANDARD_RECORD_LENGTH;
        if ($gi->flags & GEOIP_SHARED_MEMORY) {
            $offset = @shmop_size($gi->shmid) - 3;
            for ($i = 0; $i < STRUCTURE_INFO_MAX_SIZE; $i++) {
                $delim = @shmop_read($gi->shmid, $offset, 3);
                $offset += 3;
                if ($delim == (chr(255) . chr(255) . chr(255))) {
                    $gi->databaseType = ord(@shmop_read($gi->shmid, $offset, 1));
                    if ($gi->databaseType >= 106) {
                        $gi->databaseType -= 105;
                    }
                    $offset++;

                    if ($gi->databaseType == GEOIP_REGION_EDITION_REV0) {
                        $gi->databaseSegments = GEOIP_STATE_BEGIN_REV0;
                    } elseif ($gi->databaseType == GEOIP_REGION_EDITION_REV1) {
                        $gi->databaseSegments = GEOIP_STATE_BEGIN_REV1;
                    } elseif (($gi->databaseType == GEOIP_CITY_EDITION_REV0)
                        || ($gi->databaseType == GEOIP_CITY_EDITION_REV1)
                        || ($gi->databaseType == GEOIP_ORG_EDITION)
                        || ($gi->databaseType == GEOIP_ORG_EDITION_V6)
                        || ($gi->databaseType == GEOIP_DOMAIN_EDITION)
                        || ($gi->databaseType == GEOIP_DOMAIN_EDITION_V6)
                        || ($gi->databaseType == GEOIP_ISP_EDITION)
                        || ($gi->databaseType == GEOIP_ISP_EDITION_V6)
                        || ($gi->databaseType == GEOIP_USERTYPE_EDITION)
                        || ($gi->databaseType == GEOIP_USERTYPE_EDITION_V6)
                        || ($gi->databaseType == GEOIP_LOCATIONA_EDITION)
                        || ($gi->databaseType == GEOIP_ACCURACYRADIUS_EDITION)
                        || ($gi->databaseType == GEOIP_CITY_EDITION_REV0_V6)
                        || ($gi->databaseType == GEOIP_CITY_EDITION_REV1_V6)
                        || ($gi->databaseType == GEOIP_NETSPEED_EDITION_REV1)
                        || ($gi->databaseType == GEOIP_NETSPEED_EDITION_REV1_V6)
                        || ($gi->databaseType == GEOIP_ASNUM_EDITION)
                        || ($gi->databaseType == GEOIP_ASNUM_EDITION_V6)
                    ) {
                        $gi->databaseSegments = 0;
                        $buf = @shmop_read($gi->shmid, $offset, SEGMENT_RECORD_LENGTH);
                        for ($j = 0; $j < SEGMENT_RECORD_LENGTH; $j++) {
                            $gi->databaseSegments += (ord($buf[$j]) << ($j * 8));
                        }
                        if (($gi->databaseType == GEOIP_ORG_EDITION)
                            || ($gi->databaseType == GEOIP_ORG_EDITION_V6)
                            || ($gi->databaseType == GEOIP_DOMAIN_EDITION)
                            || ($gi->databaseType == GEOIP_DOMAIN_EDITION_V6)
                            || ($gi->databaseType == GEOIP_ISP_EDITION)
                            || ($gi->databaseType == GEOIP_ISP_EDITION_V6)
                        ) {
                            $gi->record_length = ORG_RECORD_LENGTH;
                        }
                    }
                    break;
                } else {
                    $offset -= 4;
                }
            }
            if (($gi->databaseType == GEOIP_COUNTRY_EDITION) ||
                ($gi->databaseType == GEOIP_COUNTRY_EDITION_V6) ||
                ($gi->databaseType == GEOIP_PROXY_EDITION) ||
                ($gi->databaseType == GEOIP_NETSPEED_EDITION)
            ) {
                $gi->databaseSegments = GEOIP_COUNTRY_BEGIN;
            }
        } else {
            $filepos = ftell($gi->filehandle);
            fseek($gi->filehandle, -3, SEEK_END);
            for ($i = 0; $i < STRUCTURE_INFO_MAX_SIZE; $i++) {
                $delim = fread($gi->filehandle, 3);
                if ($delim == (chr(255) . chr(255) . chr(255))) {
                    $gi->databaseType = ord(fread($gi->filehandle, 1));
                    if ($gi->databaseType >= 106) {
                        $gi->databaseType -= 105;
                    }
                    if ($gi->databaseType == GEOIP_REGION_EDITION_REV0) {
                        $gi->databaseSegments = GEOIP_STATE_BEGIN_REV0;
                    } elseif ($gi->databaseType == GEOIP_REGION_EDITION_REV1) {
                        $gi->databaseSegments = GEOIP_STATE_BEGIN_REV1;
                    } elseif (($gi->databaseType == GEOIP_CITY_EDITION_REV0)
                        || ($gi->databaseType == GEOIP_CITY_EDITION_REV1)
                        || ($gi->databaseType == GEOIP_CITY_EDITION_REV0_V6)
                        || ($gi->databaseType == GEOIP_CITY_EDITION_REV1_V6)
                        || ($gi->databaseType == GEOIP_ORG_EDITION)
                        || ($gi->databaseType == GEOIP_DOMAIN_EDITION)
                        || ($gi->databaseType == GEOIP_ISP_EDITION)
                        || ($gi->databaseType == GEOIP_ORG_EDITION_V6)
                        || ($gi->databaseType == GEOIP_DOMAIN_EDITION_V6)
                        || ($gi->databaseType == GEOIP_ISP_EDITION_V6)
                        || ($gi->databaseType == GEOIP_LOCATIONA_EDITION)
                        || ($gi->databaseType == GEOIP_ACCURACYRADIUS_EDITION)
                        || ($gi->databaseType == GEOIP_CITY_EDITION_REV0_V6)
                        || ($gi->databaseType == GEOIP_CITY_EDITION_REV1_V6)
                        || ($gi->databaseType == GEOIP_NETSPEED_EDITION_REV1)
                        || ($gi->databaseType == GEOIP_NETSPEED_EDITION_REV1_V6)
                        || ($gi->databaseType == GEOIP_USERTYPE_EDITION)
                        || ($gi->databaseType == GEOIP_USERTYPE_EDITION_V6)
                        || ($gi->databaseType == GEOIP_ASNUM_EDITION)
                        || ($gi->databaseType == GEOIP_ASNUM_EDITION_V6)
                    ) {
                        $gi->databaseSegments = 0;
                        $buf = fread($gi->filehandle, SEGMENT_RECORD_LENGTH);
                        for ($j = 0; $j < SEGMENT_RECORD_LENGTH; $j++) {
                            $gi->databaseSegments += (ord($buf[$j]) << ($j * 8));
                        }
                        if (($gi->databaseType == GEOIP_ORG_EDITION)
                            || ($gi->databaseType == GEOIP_DOMAIN_EDITION)
                            || ($gi->databaseType == GEOIP_ISP_EDITION)
                            || ($gi->databaseType == GEOIP_ORG_EDITION_V6)
                            || ($gi->databaseType == GEOIP_DOMAIN_EDITION_V6)
                            || ($gi->databaseType == GEOIP_ISP_EDITION_V6)
                        ) {
                            $gi->record_length = ORG_RECORD_LENGTH;
                        }
                    }
                    break;
                } else {
                    fseek($gi->filehandle, -4, SEEK_CUR);
                }
            }
            if (($gi->databaseType == GEOIP_COUNTRY_EDITION) ||
                ($gi->databaseType == GEOIP_COUNTRY_EDITION_V6) ||
                ($gi->databaseType == GEOIP_PROXY_EDITION) ||
                ($gi->databaseType == GEOIP_NETSPEED_EDITION)
            ) {
                $gi->databaseSegments = GEOIP_COUNTRY_BEGIN;
            }
            fseek($gi->filehandle, $filepos, SEEK_SET);
        }
        return $gi;
    }

}
