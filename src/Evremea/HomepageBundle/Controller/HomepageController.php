<?php

namespace Evremea\HomepageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Evremea\HomepageBundle\Utils\Utils;
use Evremea\HomepageBundle\Utils\MockUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

use Evremea\HomepageBundle\Utils\GeoIp\City as GeoIpCity;

class HomepageController extends Controller
{   
    
    CONST SECONDS_24_HOURS = 86400;
    
    private function _getGeoIpCity($request, $citySlug=null)
    {
        if (!$citySlug)
        {
            //check cookie
            $cookie = $request->cookies->get('evremea_location', null);
            if($cookie)
            {
                $geoIpCity = json_decode($cookie);
            }
            else
            {
                //else detect
                $ip = Utils::getUserIP();
                $geoIpCity = Utils::getCityByIp($ip);
            }
        } 
        else
        {
            $repository = $this->getDoctrine()->getRepository('EvremeaHomepageBundle:City');
            $cityDb = $repository->findOneByUrl($citySlug);
            if (!$cityDb)
            {
                return new Response('', 404);
            }
            $geoIpCity = new GeoIpCity();
            $geoIpCity->city = $cityDb->getName();
            $geoIpCity->country_name = '';
            $geoIpCity->region = $cityDb->getCounty() ? $cityDb->getCounty()->getName() : '';
            $geoIpCity->address_details = $cityDb->getAddressDetails();
            $geoIpCity->latitude = $cityDb->getLatitude();
            $geoIpCity->longitude = $cityDb->getLongitude();
        }
        
        if ($geoIpCity->city == NULL)
	{
	    $geoIpCity->city = $geoIpCity->region;
	}

        if ($geoIpCity->city == NULL)
	{
            $geoIpCity->city = $geoIpCity->country_name;
	}
        
        if ($geoIpCity->city == NULL)
        {
        //    $geoIpCity->city = _('locația ta');
        }
        
        if ($geoIpCity->city == 'Bucharest' || $geoIpCity == NULL)
        {
            $geoIpCity->city = 'București';
        }
        
        return $geoIpCity;
    }
    
    public function indexAction($citySlug)
    {
        $request = $this->container->get('request');
        
        $forecast = $this->get('forecast_io');
        $checkCache = $request->query->get('checkCache', true);
	$forecast->setCheckCache($checkCache);
        
        $request->setLocale('ro_RO');
        
        $biggestCities = Utils::getBiggestCities();
        $geoIpCity = $this->_getGeoIpCity($request, $citySlug);
         
        if (Utils::isBot())
        {   
            $json = MockUtils::getJson();
            $icon = Utils::getIconFromData($json);
            $forecastText = '';
            foreach ($json['daily']['data'] as &$el)
            {
                $el['summary'] = Utils::translator($el['summary']);
            }
            
            $data = array(
                'full_data' => $json,
                'city' => $geoIpCity->city, 
                'biggestCities' => $biggestCities,
                'region' => $geoIpCity->region, 
                'address_details' => $geoIpCity->address_details,
                'icon' => $icon,
                'forecastText' => $forecastText);
        
            return $this->render('EvremeaHomepageBundle:Default:index.html.twig', $data);
        }
        
        $expiration = time() + (3600 * 24 * 7); //1 week
        
        $response = new Response();
        $response->headers->setCookie(new Cookie('evremea_location', json_encode($geoIpCity), $expiration));
        $response->sendHeaders();
        
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('EvremeaHomepageBundle:ForecastioApiKeys');
        $key = $repository->getAvailableKey();
        
        if (!$key)
        {
            return $this->render('EvremeaHomepageBundle:Default:no_key.html.twig');
        }
        
	$forecast->setApiKey($key);
	
	$json = $forecast->get($geoIpCity->latitude, $geoIpCity->longitude);
        
        $apiKeyObj = $repository->findOneByApiKey($key);
        
        //increase the number of calls
        if (!$forecast->isServedFromCache())
        {
            $callsRepository = $em->getRepository('EvremeaHomepageBundle:ForecastioCalls');
            $apiCall = $callsRepository->getCallByApiIdAndDate($apiKeyObj->getId());
            if ($apiCall)
            {
                $apiCall->setCounter($apiCall->getCounter() + 1);
                $manager = $this->getDoctrine()->getManager();
                $manager->persist($apiCall);
                $manager->flush();
            }
        }
        //end increase
        
        $pos = 0;
        $found = 0;
        foreach ($json['daily']['data'] as &$el)
        {
            if ((time() - $el['time']) > self::SECONDS_24_HOURS) {
                $found = $pos;
            }
            $pos++;
            $el['summary'] = Utils::translator($el['summary']);
        }
        
        unset($json['daily']['data'][0]);
        
        $icon = Utils::getIconFromData($json);
        
        $today = date('dmY');
        $forecastFiles = $this->get('kernel')->getRootDir() . '/../forecast_files/';
        $file = $forecastFiles . $today . ".html";
        
        $forecastText = '';
        if (file_exists($file))
        {
            $forecastText = file_get_contents($file, true);
        }
        
        $data = array(
            'full_data' => $json,
            'city'=>$geoIpCity->city, 
            'biggestCities' => $biggestCities,
            'region' => $geoIpCity->region, 
            'address_details' => $geoIpCity->address_details,
            'icon' => $icon,
            'forecastText' => $forecastText);
        
        return $this->render('EvremeaHomepageBundle:Default:index.html.twig', $data);
    }
    
    public function listAction(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('EvremeaHomepageBundle:City');
        $paginator  = $this->get('knp_paginator');
        
        $cities = $repository->createQueryBuilder('a')
                ->leftJoin('a.county', 'co')->addSelect("co") 
                ->where('a.countryId=1')
                ->orderBy('a.asciiName', 'ASC');
                
        $pagination = $paginator->paginate(
            $cities,
            $request->query->get('page', 1)/*page number*/,
            30/*limit per page*/
        );
        
        return $this->render('EvremeaHomepageBundle:Default:list.html.twig', array('pagination' => $pagination));
    }
    
    public function widgetAction(Request $request)
    {
        $request->setLocale('ro_RO');
        
        $citySlug = $request->query->get('city', null);
        if (!$citySlug)
        {
            return new Response('');
        }
        
        $cityRepository = $this->getDoctrine()->getRepository('EvremeaHomepageBundle:City');
        $cityDb = $cityRepository->findOneByUrl($citySlug);
        if (!$cityDb)
        {
            return new Response('');
        }
        
        $forecast = $this->get('forecast_io');
        $forecast->setCheckCache(true);
        $em = $this->getDoctrine()->getManager();
        $forecastRepository = $em->getRepository('EvremeaHomepageBundle:ForecastioApiKeys');
        $key = $forecastRepository->getAvailableKey();
        
        if (!$key)
        {
            return new Response('');
        }
        $forecast->setApiKey($key);
        
        $json = $forecast->get($cityDb->getLatitude(), $cityDb->getLongitude());
        
        foreach ($json['daily']['data'] as &$el)
        {
            $el['summary'] = Utils::translator($el['summary']);
        }
        
        $icon = Utils::getIconFromData($json);
        
        $data = array(
            'full_data' => $json,
            'city' => $cityDb->getName(), 
            'slug' => $citySlug,
            'icon' => $icon);
        
        return $this->render('EvremeaHomepageBundle:Default:widget.html.twig', $data);
    }
    
    public function apiIndexAction(Request $request)
    {
        $request->setLocale('ro_RO');
        $citySlug = $request->query->get('city', null);
        
        $response = $this->widgetAction($request);
        
        $data = array(
            'weatherText' => $response->getContent()
        );
        return $this->render('EvremeaHomepageBundle:Default:api_index.html.twig', $data);
        
    }
    
    public function onYourWebsiteAction(Request $request)
    {
        $request->setLocale('ro_RO');
        
        $data = array();
        return $this->render('EvremeaHomepageBundle:Default:on-your-website.html.twig', $data);
    }
    
    public function bulgarianSeasideAction(Request $request) 
    {
        $request->setLocale('ro_RO');
        
        $urls = array(
            'nisipurile-de-aur' => 'Nisipurile de aur', 
            'constantin-si-elena' => 'Constantin și Elena', 
            'sunny-beach-bulgaria' => 'Sunny Beach', 
            'albena' => 'Albena', 
            'balchik' => 'Balcic',
            'krapets'  => 'Krapeț',
            'varna' => 'Varna');
        $data = array('urls' => $urls);
        
        return $this->render('EvremeaHomepageBundle:Default:bulgarian-seaside.html.twig', $data);
    }

    public function romanianMountainAction(Request $request)
    {
        $request->setLocale('ro_RO');
        $urls = array(
            'arieseni' => 'Arieșeni (Jud. Alba)',
            'azuga' => 'Azuga (Jud. Prahova)',
            'baile-tusnad' => 'Băile Tușnad (Jud. Harghita)',
            'baisoara' => 'Băișoara (Jud. Cluj)',
            'poiana-brasov' => 'Poiana Brasov (Jud. Prahova)',
            'borsec' => 'Borsec (Jud. Harghita)',
            'busteni-1' => 'Bușteni (Jud. Prahova)',
            'lacul-balea' => 'Lacul Bâlea (Jud. Sibiu)',
            'sinaia' => 'Sinaia (Jud. Prahova)',
            'paltinis' => 'Păltiniș (Jud. Sibiu)',
            'vatra-dornei' => 'Vatra Dornei (Jud. Suceava)',
        );

        $data = array('urls' => $urls);

        return $this->render('EvremeaHomepageBundle:Default:romanian-mountains.html.twig', $data);
    }   
 
    public function romanianSeasideAction(Request $request) 
    {
        $request->setLocale('ro_RO');
        
        $urls = array(
            'mamaia' => 'Mamaia',
            'navodari' => 'Năvodari',
            'corbu' => 'Corbu',
            'constanta' => 'Constanța',
            'faleza-constanta' => 'Faleza Constanța',
            'eforie-nord'=>'Eforie Nord',
            'eforie-sud'=>'Eforie Sud',
            'tuzla' => 'Tuzla',
            'costinesti' => 'Costinești',
            'olimp' => 'Olimp',
            'neptun' => 'Neptun',
            'jupiter' => 'Jupiter',
            'venus' => 'Venus',
            'saturn' => 'Saturn',
            'mangalia' => 'Mangalia',
            '2-mai' => '2 Mai',
            'vama-veche' => 'Vama Veche',
        );
        
        $data = array('urls' => $urls);
        
        return $this->render('EvremeaHomepageBundle:Default:romanian-seaside.html.twig', $data);
    }
    
    public function cookiesAction(Request $request) {
        $request->setLocale('ro_RO');
        
        $data = array();
        return $this->render('EvremeaHomepageBundle:Default:cookies.html.twig', $data);
    }

    public function alerteAction(Request $request) {
        $request->setLocale('ro_RO');
        
        $rss = simplexml_load_file('http://www.meteoromania.ro/avertizari-rss.php');

        return $this->render('EvremeaHomepageBundle:Default:alerte.html.twig', array(
                'rss' => $rss,
        ));
    }    
}
