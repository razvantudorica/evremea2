<?php

namespace Evremea\HomepageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends Controller
{
    public function searchAjaxAction()
    {
        $request = $this->container->get('request');
        
        if (!$request->isXmlHttpRequest())
        {
            return new Response('', 400); 
        }
        
        $string = $request->query->get('city');
        
        $repository = $this->getDoctrine()->getRepository('EvremeaHomepageBundle:City');
        
        $cities = $repository->createQueryBuilder('c')
                ->where('c.asciiName LIKE :city')
                ->setParameter('city', '%'.$string.'%')
                ->getQuery()
                ->getResult();
        
        $response = array();
        foreach ($cities as $city)
        {
            $cityUrl = $this->generateUrl('city_page', array('citySlug' => $city->getUrl()));
            
            $region = '';
            if ($city->getCounty())
            {
                $region = $city->getCounty()->getName();
            }
            
            $response[] = array('name' => $city->getAsciiName(), 
                'region' => $region, 
                'url' => $cityUrl,
                'slug' => $city->getUrl());
        }
        
        return new Response(json_encode($response)); 
    }
}
