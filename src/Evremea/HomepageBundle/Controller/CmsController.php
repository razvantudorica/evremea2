<?php

namespace Evremea\HomepageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Evremea\HomepageBundle\Utils\Utils;
use Evremea\HomepageBundle\Utils\MockUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;


class CmsController extends Controller
{
    public function indexAction()
    {
        $data = array();
        return $this->render('EvremeaHomepageBundle:Cms:prognoza-ianuarie.html.twig', $data);
    }
}