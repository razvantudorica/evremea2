<?php

namespace Evremea\ForecastBundle\Forecast;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

class ForecastIo
{
    
    private $httpClient;
    private $mocked;
    private $logger;
    private $cache;
    private $checkCache;
    
    public function __construct($baseUrl, $httpClient, $cache, LoggerInterface $logger) 
    {
        $this->httpClient = $httpClient;        
        $this->httpClient->setBaseUrl($baseUrl);
        $this->units = 'si';  // Can be set to 'us', 'si', 'ca', 'uk' or 'auto' (see forecast.io API); default is auto
        $this->language = '';
        $this->mocked = False;
        $this->logger = $logger;
        $this->cache = $cache;
        $this->checkCache = true;
        $this->servedFromCache = false;
    }
    
    public function setCheckCache($flag)
    {
        $this->checkCache = $flag;
    }
    
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }
    
    public function setMocked($mocked=True)
    {
        $this->mocked = $mocked;
    }
    
    public function isServedFromCache()
    {
        return $this->servedFromCache;
    }
    
    public function get($latitude, $longitude, $timestamp=null, $exclusions=null)
    {
        /**
         * It calls the forecast.io API or get the data from cache.
         * Returns an associtive array with the weather data
         * 
         */
        
        if ($this->mocked)
        {
            return $this->getMockedJson();
        }
        
        $cacheKey = $latitude . '_' . $longitude;
        
        if ($this->checkCache)
        {
            //check cache
            $data = $this->cache->fetch($cacheKey);
            if ($data)
            {
                $this->servedFromCache = true;
                return json_decode($data, true);
            }
        }
        
        $resource = $this->apiKey . '/'. $latitude .',' . $longitude . '?units=' . $this->units . '&lang=' . $this->language .
        ( $timestamp ? ',' . $timestamp : '' ) .
        ( $exclusions ? '&exclude=' . $exclusions : '' );
        
        $request = $this->httpClient->get($resource);
        $res = $request->send();
        if ($res->getStatusCode() != 200)
        {
            $this->logger->error("Could not fetch the response: " . 
                    $res->getStatusCode() . ", " . var_export($res, 1));
            throw new \Exception("Could not fetch the response");
        }
        
        $data = $res->json();
        
        $this->logger->info("Called: " . $resource);
        
        $cachedData = json_encode($data);
        $this->cache->save($cacheKey, $cachedData, 3600 * 2);
        
        return $data;
    }
    
    private function getMockedJson()
    {
        $string = file_get_contents("/home/razvan/dev/evremea2/data/response.json");
        $json_a = json_decode($string, true);

        return $json_a;
    }
}