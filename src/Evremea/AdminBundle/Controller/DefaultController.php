<?php

namespace Evremea\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function ForecastListAction()
    {
        $request = $this->container->get('request');
        $day = $request->query->get('day', date('dmY'));
        
        $forecastFiles = $this->get('kernel')->getRootDir() . '/../forecast_files/';
        $file = $forecastFiles . $day . ".html";
        
        $content = '';
        if (file_exists($file)) {
            $content = file_get_contents($file, true);
           
        }
        
        if ($request->getMethod() == 'POST') {
            $content = $request->request->get('content');
            file_put_contents($file, $content);
        }
        
        return $this->render('EvremeaAdminBundle:Default:forecast-list.html.twig', array('content' => $content, 'day' => $day));
    }
}
