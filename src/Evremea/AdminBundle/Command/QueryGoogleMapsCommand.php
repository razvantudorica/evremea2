<?php
namespace Evremea\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

ini_set('memory_limit', '-1');

class QueryGoogleMapsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('google:update-region')
            ->setDescription('Query Google Maps API and update our DB with the correct regions by lat/long according with Google')
            ->addArgument('max', InputArgument::OPTIONAL, 'Maximum number of cities to check')
//            ->addArgument('longitude', InputArgument::OPTIONAL, 'Longitude')
//            ->addOption('yell', null, InputOption::VALUE_NONE, 'If set, the task will yell in uppercase letters')
        ;
        
        
    }
    
     
    private function _getCounties($countryId = 1)
    {
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager();
        $repository = $this->em->getRepository('EvremeaHomepageBundle:County');
        $counties = $repository->findAll();
        return $counties;
    }
    
    private function _getUnparsedCities($countryId = 1, $max=100)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $repository = $em->getRepository('EvremeaHomepageBundle:City');
        
        $qb = $repository->createQueryBuilder('c')
                    ->where('c.countryId=1')
		    ->andWhere('c.googleParsed = 0')
                    ->andWhere('c.county is null')
                    ->getQuery()
                    ->setMaxResults($max)
                ;
        
        $cities = $qb->getResult();
        
        return $cities;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $max = $input->getArgument('max', 100);
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $countryRepository = $em->getRepository('EvremeaHomepageBundle:County');
        
        $httpClient = $this->getContainer()->get('guzzle.client');
            
        $cities = $this->_getUnparsedCities(1, $max);
        
        $countiesSimple = array();
        $counties = $this->_getCounties();
        
        
        foreach ($counties as $county)
        {
            if ($county->getCountry()->getId()!=1)
            {
                continue;
            }
            $countiesSimple[$county->getId()] = str_replace(array('Județul ', 'Municipiul '), '', $county->getName());
        }
        
        $baseUrl = 'https://maps.googleapis.com';
        $httpClient->setBaseUrl($baseUrl);

        $total = 0;
        foreach ($cities as $city)
	{
            if ($total >= 2500)
	    {
                die('Number of queries per day finished');

	    }
            if ($city->getCounty())
            {
                continue;
            }
            
            $output->writeln("Looking for: ".$city->getName() . 
                    " ID: ". $city->getId() . 
                    " Coordinates: " . $city->getLatitude() . 
                    ", " . $city->getLongitude());
            
            $resource = "/maps/api/geocode/json?latlng=".
                    $city->getLatitude(). 
                    "," . 
                    $city->getLongitude() . 
                    "&key=AIzaSyDMXo2lxVBMoyRn3TOicIK4-7lQbenpzCU";
            

            $request = $httpClient->get($resource);
            $res = $request->send();
            if ($res->getStatusCode() != 200)
            {
                $output->write('Could not get response');
                var_dump($res);
            }
            $total++;

            $data = $res->json();
            $details = '';
            foreach ($data['results'][0]['address_components'] as $component)
            {
                if ('România' == $component['long_name'] || 'Romania' == $component['long_name'])
                {
                    continue;
                }

                $long_name = str_replace(array('Județul ', 'Judetul '), '', $component['long_name']);

                $key = array_search($long_name, $countiesSimple);
                if ($key !== false) {
                    $output->writeln("Setting county: " . $key . ", " . $long_name );
                    
                    $city->setCounty($countryRepository->find($key));
                    
                }
                else
                {
                    $details .= $component['long_name'] . ', ';
                }
            }
            
            $details = rtrim($details, ', ');
            
            $output->writeln($details);
            
            
            $city->setGoogleParsed(1);
            $city->setAddressDetails($details);
            
            $em->persist($city);
            $em->flush();
        }

        $output->writeln('OK');
    }
}
