Snow (4–9 cm.) throughout the day.
Light snow (1–2 cm.) until evening.
Light snow in the morning.
Partly cloudy starting in the afternoon.
Light snow (under 1 cm.) in the morning and evening and breezy overnight.
Rain in the morning and evening.
Snow (1–3 cm.) in the morning and evening.
Light snow (under 1 cm.) starting in the evening.
Partly cloudy throughout the day.
Light rain throughout the day.
Drizzle in the afternoon.
Light rain and breezy until afternoon.  
Light snow (1–2 cm.) and breezy until afternoon.
Mostly cloudy starting in the evening.
Light rain overnight.
Light rain starting in the afternoon, continuing until evening.
Flurries in the morning.
Windy starting in the afternoon and light snow starting in the afternoon, continuing until evening.
Breezy in the morning.
Breezy and mostly cloudy in the morning.
Light rain in the morning and evening.
Breezy in the morning and mostly cloudy throughout the day.
Drizzle until afternoon.
Snow (6–13 cm.) throughout the day.
Light snow (2–5 cm.) throughout the day.
Light snow (under 1 cm.) until afternoon.
Light rain until afternoon, starting again in the evening.
Light snow overnight.
