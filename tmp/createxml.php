<?php

if (count($argv) == 1)
{
  $start = 1;
}
else
{
  $start = $argv[1];
}

$file = fopen("text.txt", "r");
$i = $start;
$xml = '';
while (!feof($file)) {
  $line = fgets($file);
  $xml .= "<trans-unit id=\"$i\">\n";
  $xml .= "    <source>".trim($line) ."</source>\n";
  $xml .= "    <target></target>\n"; 
  $xml .= "</trans-unit>\n"; 
  $i++;
}


fclose($file);

echo $xml;

?>
